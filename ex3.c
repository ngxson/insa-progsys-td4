#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 100

void print_help();
void print_error();

int main(int argc, char *argv[]) {
  char *my_file = argv[1];
  char buffer[BUFFER_SIZE], c, *ptr;
  struct stat st;

  if (argc < 3) {
    print_help();
    return 0;
  }

  // parse int from argv
  int nb_char_to_read = strtol(argv[2], &ptr, 10);

  // firstly, get file size
  int err = stat(my_file, &st);
  // stat error
  if (err) {
    print_error();
    return err;
  }
  int size = st.st_size;

  // then, open the file
  int fd = open(my_file, O_RDONLY);

  // skip to expected location
  int start_loc = size - nb_char_to_read;
  start_loc = start_loc < 0 ? 0 : start_loc; // negative nb is not allowed
  lseek(fd, start_loc, SEEK_SET);

  // read data
  int nb_of_read_char; // red() returns the number of bytes read
  while ((nb_of_read_char = read(fd, buffer, BUFFER_SIZE - 1)) > 0) {
    buffer[nb_of_read_char] = '\0'; // indicate end of string
    printf("%s", buffer);
  }

  // read error
  if (nb_of_read_char < 0) {
    print_error();
    return nb_char_to_read;
  }

  // clean up
  close(fd);
  printf("\n");

  return 0;
}

void print_help() {
  printf("\
Usage: ex3 [file] [number of char to read]\n \
Example: ex3 ./test.txt 10\n \
  ");
}

void print_error() {
  perror("Error");
}

/*

gcc ./ex3.c -o ex3 && ./ex3 ./test.txt 10

*/